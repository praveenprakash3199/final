const express = require("express");
const bodyParser = require("body-parser");
const mongoUtils= require('./config/mongoUtils');
const {UserModel} = require('./schema/user.schema');
const {BlogModel} = require('./schema/blog.schema');
const {CommentsModel} = require('./schema/comments.schema');
// const { response } = require("express");
const app= express();


app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({extended:true}));
mongoUtils.mongooseConnect();

//user
// app.get("/users", (req,res)=>{
//     UserModel.find((err,docs)=>{
//         if(err) console.log(err);
//         return res.send(docs);
//     })
// })



app.post("/users",(req,res)=>{ 
   if(req.body){
       console.log(req.body);
       const UsersItem = new UserModel(req.body);
       UsersItem.save()
       .then(response=>{
            return res.send(response);
       })
       .catch(err=>console.log(err));
   }
})

// app.get("/users/:userId",(req,res)=>{
//     if(req.params){
//         console.log(req.params);
//         const userId =req.params.userId;
//         UserModel.findOne({userId})
//             .then(response=>{
//                 return res.send(response);
               
//             }).catch(err=> console.log(err))
//     }
   
// })

//blog
app.get("/blog", (req,res)=>{
    BlogModel.find((err,docs)=>{
        if(err) console.log(err);
        return res.send(docs);
    })
})



app.post("/blog",(req,res)=>{ 
   if(req.body){
       console.log(req.body);
       const BlogItem = new BlogModel(req.body);
       BlogItem.save()
       .then(response=>{
            return res.send(response);
       })
       .catch(err=>console.log(err));
   }
})

app.get("/blog/:blogId",(req,res)=>{
    if(req.params){
        console.log(req.params);
        const blogId =req.params.blogId;
        BlogModel.findOne({blogId})
            .then(response=>{
                return res.send(response);
               
            }).catch(err=> console.log(err))
    }
   
})

//comments
app.get("/comments", (req,res)=>{
    CommentsModel.find((err,docs)=>{
        if(err) console.log(err);
        return res.send(docs);
    })
})



app.post("/comments",(req,res)=>{ 
   if(req.body){
       console.log(req.body);
       const CommentsItem = new CommentsModel(req.body);
       CommentsItem.save()
       .then(response=>{
            return res.send(response);
       })
       .catch(err=>console.log(err));
   }
})

app.get("/comments/:commentId",(req,res)=>{
    if(req.params){
        console.log(req.params);
        const commentId =req.params.commentId;
        CommentsModel.findOne({commentId})
            .then(response=>{
                return res.send(response);
               
            }).catch(err=> console.log(err))
    }
   
})



//  -/api/users 
//- displays all Users along with their posted Blogs and Comments
app.get("/users", (req,res)=>{
    UserModel.find((err,docs)=>{
        BlogModel.find((err1,docs1)=>{
            CommentsModel.find((err2,docs2)=>{
                return res.send(docs+docs1+docs2)
            })

        })        
    })

})


// /api/users/{userId} 
// - displays the User along with his posted Blogs and Comments 
app.get("/users/:userId",(req,res)=>{
    if(req.params){
        // console.log(req.params);
        const userId =req.params.userId;
        UserModel.findOne({userId}) 
            .then(response=>{
              
                BlogModel.find({userId})
                .then(response1=>{
                    // console.log(response1);
                    CommentsModel.findOne({userId})
                    .then(response2=>{
                        // console.log(response2);
                        return res.send(response+response1+response2);
                    })
                })
                
               
            }).catch(err=> console.log(err))
    }
   
})

/* /api/users/{userId}/blogs 
- displays the Blogs posted by the given User */
app.get("/users/:userId/blogs",(req,res)=>{
    if(req.params){
        // console.log(req.params);
        const userId =req.params.userId;
        UserModel.findOne({userId}) 
            .then(response=>{
                BlogModel.find({userId})
                .then(response1=>{
                        return res.send(response+response1);
                })
            }).catch(err=> console.log(err))
    }
})


/* /api/users/{userId}/blogs/{blogId}/comments 
- displays all the Comments for the given Blog for the given User */
app.get("/users/:userId/blogs/:blogId/comments",(req,res)=>{
    if(req.params){
        const userId =req.params.userId;
        const blogId = req.params.blogId
        CommentsModel.find({blogId})
        .then(response=>{
            BlogModel.find({userId})
            .then(response1=>{
               return res.send(response)
                
            })
        })
    }
})

/* /api/users/{userId}/blogs/{blogId}/comments/{commentId} 
- displays the Comment for the given Blog for the given User */
app.get('/users/:userId/blogs/:blogId/comments/:commentId',(req,res)=>{
    if(req.params){
        const userId =req.params.userId;
        const blogId = req.params.blogId;
        const commentId= req.params.commentId;
        CommentsModel.find({commentId})
        .then(response=>{
            BlogModel.find({blogId})
            .then(response1=>{
               UserModel.find({userId})
               .then(response2=>{
                   return res.send(response)
               })
            })
        })
    }
})

/* /api/blogs/{blogId}/comments/{commentId}
- displays the Comment for the given Blog */

app.get('/blogs/:blogId/comments/:commentId',(req,res)=>{
    if(req.params){
        const blogId = req.params.blogId;
        const commentId= req.params.commentId;
        CommentsModel.find({commentId})
        .then(response=>{
            return res.send(response)
        })
    }
})





app.listen(9001,()=>{
    console.log("Server is started at port 9001");
})

