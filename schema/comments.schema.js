const mongoose = require("mongoose");


const commentsSchema=  mongoose.Schema({
    commentId : {
        type : String,  
         required : true,   
        
    },
      text:{
         type: String,
         required : true,  
     },
     blogId:{
         type:String,
         required:true
     },
     
     userId:{
        type:String,
        required:true
    }

})

const CommentsModel = mongoose.model("Comments",commentsSchema);
module.exports={CommentsModel};