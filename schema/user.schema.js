const mongoose = require("mongoose");
const validator = require("validator");

const userSchema=  mongoose.Schema({
    userId : {
        type : String,  
         required : true,   
        unique:true
    },
      userName:{
         type: String,
         required : true,  
     },
     email: {
        type: String,
        required: true,
        lowercase:true,
        validate(value) {
          if (!validator.isEmail(value)) {
            throw new Error("email is not valid");
          }
        }
      }

})

const UserModel = mongoose.model("Users",userSchema);
module.exports={UserModel};