const mongoose = require("mongoose");


const blogSchema=  mongoose.Schema({
    blogId : {
        type : String,  
         required : true,   
    },
      title:{
         type: String,
         required : true,  
     },
     body:{
         type:String,
         required:true,
     },
     published:{
         type:Boolean,
         required:true,
     },
     userId:{
         type:String,
         required:true,
     }
})

const BlogModel = mongoose.model("Blogs",blogSchema);
module.exports={BlogModel};